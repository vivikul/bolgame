/**
 * 
 */
package com.bol.game.domain;

/**
 * Class to Keep the configuration values of the board
 * Initialized to default values, immutable class
 * 
 * @author Vishal
 *
 */
public final class BoardConfiguration {
	
	private final int noOfPlayers;
	
	private final int numOfPitsPerPlayer;
	
	private final int numOfPebblesPerPit;

	/**
	 * Default constructor initialized to 2,6,6
	 */
	public BoardConfiguration() {
		this(2,6,6);
	}
	
	public BoardConfiguration(int numOfPlayers, int numOfPits, int numOfStones) {
		this.noOfPlayers = numOfPlayers;
		this.numOfPitsPerPlayer = numOfPits;
		this.numOfPebblesPerPit = numOfStones;
	}

	public int getNoOfPlayers() {
		return noOfPlayers;
	}

	public int getNumOfPitsPerPlayer() {
		return numOfPitsPerPlayer;
	}

	public int getNumOfPebblesPerPit() {
		return numOfPebblesPerPit;
	}

}
