/**
 * 
 */
package com.bol.game.domain;

import com.bol.game.domain.exception.BuilderException;
import com.bol.game.domain.player.GamePlayer;
import com.bol.game.domain.player.Player;


/**
 * @author Vishal
 *
 */
public class GameBoardBuilder {

	private GamePlayer[] gamePlayers;
	
	private int numOfPlayers = 0;

	private BoardConfiguration boardConfiguration;
	
	/**
	 * Default constructor
	 */
	public GameBoardBuilder(){
		boardConfiguration = new BoardConfiguration();
		gamePlayers = new GamePlayer[boardConfiguration.getNoOfPlayers()];
	}

	/**
	 * Constructor that takes custom parameters
	 */
	public GameBoardBuilder(int numOfPlayers, int numOfPits, int numOfStones){
		//TODO implement it
		throw new RuntimeException("Yet to be implemented");
	}

	public GameBoardBuilder setGamePlayer(String name, int playerNum){
		if (numOfPlayers >= boardConfiguration.getNoOfPlayers())
			throw new BuilderException("Cannot add more Players ");
		Player player = new Player();
		player.setName(name);
		GamePlayer gamePlayer = new GamePlayer(playerNum, name);
		gamePlayers[numOfPlayers++] = gamePlayer;
		return this;
	}

	public GameBoard build(){
		
		verify();
		
		DefaultGameBoard gameBoard = new DefaultGameBoard(boardConfiguration, gamePlayers);
		
		return gameBoard;
	}

	private void verify(){
		boolean allPlayersInitialized = true;
		for(GamePlayer gPlayer : gamePlayers){
			if (gPlayer == null) {
				allPlayersInitialized = false;
				break;
			}
		}
		if (!allPlayersInitialized) 
			throw new BuilderException("All Players are not set");
	}
}

