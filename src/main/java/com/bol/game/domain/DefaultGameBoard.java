/**
 * 
 */
package com.bol.game.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.bol.game.domain.exception.BoardCorruptedException;
import com.bol.game.domain.exception.InvalidMoveException;
import com.bol.game.domain.exception.InvalidSelectionException;
import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;
import com.bol.game.domain.rule.DefaultGameRule;
import com.bol.game.domain.rule.ExecutionResult;
import com.bol.game.domain.rule.GameRule;

/**
 * @author Vishal
 * 
 */
public class DefaultGameBoard implements GameBoard {

	/**
	 * Unique identifier of the game
	 */
	private final long gameId;

	/**
	 * Configuration for the board
	 */
	private final BoardConfiguration configuration;

	/**
	 * The game rule implementation class
	 */
	private final GameRule gameRule;

	/**
	 * Array referencing the pits of each player Sequenced, 0th index has
	 * pitHolder for player1 and so on.
	 */
	private final List<PitHolder> pitHolders;

	/**
	 * maintains the active player, who can place the move
	 */
	private GamePlayer activePlayer;

	/**
	 * Default constructor, Sets the game rule to DefaultGameRule Initializes
	 * the pitHolders as per the default configuration and the players passed
	 * 
	 * @param gameRule
	 * @param gamePlayers
	 */
	DefaultGameBoard(BoardConfiguration boardConfiguration,
			GamePlayer gamePlayers[]) {
		
		this.configuration = boardConfiguration;
		this.gameRule = new DefaultGameRule();

		this.pitHolders = new ArrayList<PitHolder>(this.configuration.getNoOfPlayers());
		
		for (int i = 0; i <= this.configuration.getNoOfPlayers() - 1; i++) {
			this.pitHolders.add(new PitHolder(
					this.configuration.getNumOfPitsPerPlayer() + 1,
					this.configuration.getNumOfPebblesPerPit(), gamePlayers[i]));

			this.activePlayer = gamePlayers[0];

		}
		this.gameId = System.currentTimeMillis();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bol.game.domain.AbstractGameBoard#getActivePlayer()
	 */
	@Override
	public GamePlayer getActivePlayer() {
		return (GamePlayer) CloneUtility.clone(this.activePlayer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bol.game.domain.AbstractGameBoard#getBoardStatus()
	 */
	@Override
	public BoardStatus getBoardStatus() {
		BoardStatus boardStatus = new BoardStatus();
		boardStatus.setActivePlayer(getActivePlayer());
		boardStatus.setGameId(this.gameId);
		boardStatus.setPitHolders((List<PitHolder>) CloneUtility.clone(this.pitHolders));
		 boardStatus.setStatus(isEndOfGame()? GameStatus.FINISHED : GameStatus.INPROGRESS);
		 boardStatus.setWinner(isEndOfGame()? getWinner() : null);
		return boardStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bol.game.domain.AbstractGameBoard#isEndOfGame()
	 */
	@Override
	public boolean isEndOfGame() {
		for(PitHolder pHolder : pitHolders){
			if (pHolder.areAllPitsEmpty()) return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bol.game.domain.AbstractGameBoard#getWinner()
	 */
	@Override
	public GamePlayer getWinner() {
		if (!isEndOfGame()) return null;
		Collections.sort(this.pitHolders, new Comparator<PitHolder>() {
			@Override
			public int compare(PitHolder o1, PitHolder o2) {
		        int x = o1.getTotalNumOfStones();
		        int y = o2.getTotalNumOfStones();
				return (x < y) ? 1 : ((x == y) ? 0 : -1);	
			};
		});
		return this.pitHolders.get(0).getGamePlayer();
	}

	@Override
	public void play(GamePlayer player, int selectedPit) {

		validateMoveInput(pitHolders, player, selectedPit);
		
		ExecutionResult result = gameRule.executeTheMove(this.pitHolders,
				this.activePlayer, selectedPit);
		
		activePlayer = result.getNewActivePlayer();
	}

	@Override
	public long getGameId() {
		return this.gameId;
	}

	@Override
	public GamePlayer[] getPlayers() {
		
		GamePlayer gPlayers[] = new GamePlayer[configuration.getNoOfPlayers()];
		int index = 0;
		for (PitHolder pHolder : pitHolders) {
			gPlayers[index++] = (GamePlayer)CloneUtility.clone(pHolder.getGamePlayer());

		}
		return gPlayers;
	}

	private void validateMoveInput(List<PitHolder> pitHolders, GamePlayer player, int selectedPitIndex){
		if (!player.equals(this.activePlayer))
			throw new InvalidMoveException("invalid move, the player cannot place a move");

		int playerIndex = player.getPlayerIndex();
		
		if(!(pitHolders.get(playerIndex).getGamePlayer().getPlayerNumber() == player.getPlayerNumber()))
			throw new BoardCorruptedException("Board is corrupted");

		if((playerIndex  < 0 ) || (playerIndex  >= pitHolders.size()))
			throw new InvalidSelectionException("Player is not valid");
		
		PitHolder activePlayersPitHolder = pitHolders.get(playerIndex);
		
		if((selectedPitIndex  < 0) || (selectedPitIndex >= activePlayersPitHolder.getPits().size()-1)) 
			throw new InvalidSelectionException("Selected Pit is not valid");
	}

}
