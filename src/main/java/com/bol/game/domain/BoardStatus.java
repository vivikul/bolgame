/**
 * 
 */
package com.bol.game.domain;

import java.util.List;

import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 * Encapsulates all the information of the GameBoard that should be shared/ visible outside the domain
 * Eg. pass this object to the UI controller etc.
 */
public class BoardStatus {

	private long gameId;
	
	private List<PitHolder> pitHolders;
	
	private GamePlayer activePlayer;

	private GameStatus status;
	
	private GamePlayer winner;
	
	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public GamePlayer getActivePlayer() {
		return activePlayer;
	}

	public void setActivePlayer(GamePlayer activePlayer) {
		this.activePlayer = activePlayer;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	public GamePlayer getWinner() {
		return winner;
	}

	public void setWinner(GamePlayer winner) {
		this.winner = winner;
	}

	public List<PitHolder> getPitHolders() {
		return pitHolders;
	}

	public void setPitHolders(List<PitHolder> pitHolders) {
		this.pitHolders = pitHolders;
	}
	
}
