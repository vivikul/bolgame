/**
 * 
 */
package com.bol.game.domain.rule;

/**
 * @author m06i546
 *
 */
public class StoneDistributionResult {

	private boolean lastPitBig;
	
	private boolean lastPitempty;
	
	private int lastPitIndex;

	/**
	 * @return the lastPitBig
	 */
	public boolean isLastPitBig() {
		return lastPitBig;
	}

	/**
	 * @param lastPitBig the lastPitBig to set
	 */
	public void setLastPitBig(boolean lastPitBig) {
		this.lastPitBig = lastPitBig;
	}

	/**
	 * @return the lastPitempty
	 */
	public boolean isLastPitempty() {
		return lastPitempty;
	}

	/**
	 * @param lastPitempty the lastPitempty to set
	 */
	public void setLastPitempty(boolean lastPitempty) {
		this.lastPitempty = lastPitempty;
	}

	/**
	 * @return the lastPitIndex
	 */
	public int getLastPitIndex() {
		return lastPitIndex;
	}

	/**
	 * @param lastPitIndex the lastPitIndex to set
	 */
	public void setLastPitIndex(int lastPitIndex) {
		this.lastPitIndex = lastPitIndex;
	}

}
