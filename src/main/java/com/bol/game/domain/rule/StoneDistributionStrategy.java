/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.List;

import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 * Interface to abstract the shuffling strategy after making a move
 */
public interface StoneDistributionStrategy {

	/**
	 * 
	 * @param pitHolders
	 * @param player
	 * @param selectedPitIndex
	 * @return
	 */
	StoneDistributionResult shuffleStones(List<PitHolder> pitHolders, GamePlayer player, int selectedPitIndex);

}
