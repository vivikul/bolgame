/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.List;

import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 * Default implementation of GameRuleInterface
 */
public class DefaultGameRule implements GameRule{

	private StoneDistributionStrategy stoneDistributionStrategy = new StoneDistributionStrategy1();
	
	public void setStoneDistributionStrategy(StoneDistributionStrategy strategy){
		this.stoneDistributionStrategy = strategy;
	}
	
	public ExecutionResult executeTheMove(List<PitHolder> pitHolders, GamePlayer player, int selectedPitIndex){

		ExecutionResult executionResult = new ExecutionResult();

		StoneDistributionResult result = stoneDistributionStrategy.shuffleStones(pitHolders, player, selectedPitIndex);

		//if the last stone lands in big pit
		if(result.isLastPitBig()){
			result.setLastPitempty(false);
			executionResult.setNewActivePlayer(player);
		} else {
			//else move the active player to next
			int playerIndex = player.getPlayerIndex();
			playerIndex  = ++playerIndex % pitHolders.size();
			executionResult.setNewActivePlayer(pitHolders.get(playerIndex).getGamePlayer());
		}
		
		//if the last stone lands in a small pit that is empty get all the stones
		//from opponents corresponding pit
		if(result.isLastPitempty() && !result.isLastPitBig()){
			captureOpponentsStones(pitHolders,player,result.getLastPitIndex());
		}
		
		return executionResult;
	}
	
	public void captureOpponentsStones(List<PitHolder> pitHolders, GamePlayer player, int lastPitIndex){
		
		int playerIndex = player.getPlayerIndex();
		PitHolder activePlayersPitHolder = pitHolders.get(playerIndex);
		int numOfStonesInOpponentsPit = 0;
		int indexOfOpponentsPit = 0;
		for (PitHolder pitHolder: pitHolders){
			if(pitHolder.getGamePlayer().getPlayerNumber() != activePlayersPitHolder.getGamePlayer().getPlayerNumber()){
				indexOfOpponentsPit = Math.abs(lastPitIndex - (pitHolder.getPits().size() - 2));
				numOfStonesInOpponentsPit = + pitHolder.getPit(indexOfOpponentsPit).emptyThePit();
			}
		}
		activePlayersPitHolder.getPit(lastPitIndex).addStone(numOfStonesInOpponentsPit);
	}

}
