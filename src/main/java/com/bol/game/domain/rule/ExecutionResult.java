/**
 * 
 */
package com.bol.game.domain.rule;

import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 *
 */
public class ExecutionResult {

	private GamePlayer newActivePlayer;

	public GamePlayer getNewActivePlayer() {
		return newActivePlayer;
	}

	public void setNewActivePlayer(GamePlayer newActivePlayer) {
		this.newActivePlayer = newActivePlayer;
	}
	
}
