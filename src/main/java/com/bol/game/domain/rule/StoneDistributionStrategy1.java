/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.List;

import com.bol.game.domain.exception.InvalidSelectionException;
import com.bol.game.domain.pit.Pit;
import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 * 
 *
 */
public class StoneDistributionStrategy1 implements StoneDistributionStrategy {

	/* (non-Javadoc)
	 * @see com.bol.game.domain.rule.StoneDistributionStrategy#shuffleStones(com.bol.game.domain.PitHolder[], int, int, int)
	 * 
	 */
	public StoneDistributionResult shuffleStones(List<PitHolder> pitHolders, GamePlayer player,int selectedPitIndex) {
		
		StoneDistributionResult result = new StoneDistributionResult();

		validateInputForShuffling(pitHolders,player,selectedPitIndex);
		
		int playerIndex = player.getPlayerIndex();
		
		PitHolder activePlayersPitHolder = pitHolders.get(playerIndex);
		
		//pick all the stones in the selected pit
		int numOfStones = activePlayersPitHolder.getPit(selectedPitIndex).emptyThePit();

		selectedPitIndex++;

		while(numOfStones > 0){

			Pit currentPit = activePlayersPitHolder.getPits().get(selectedPitIndex);
			//is the current pit empty
			result.setLastPitempty(currentPit.isEmpty());
			//add a stone in the current pit
			currentPit.addStone();
			//is the pit big pit
			result.setLastPitBig(currentPit.isBigPit());
			//reduce by 1
			--numOfStones;
			result.setLastPitIndex(selectedPitIndex);
			//set to the first pit (circular)
			if(++selectedPitIndex == activePlayersPitHolder.getPits().size() ) selectedPitIndex = 0 ;
		}
		
		return result;
	}

	private void validateInputForShuffling(List<PitHolder> pitHolders, GamePlayer player,int selectedPitIndex){
		
		int playerIndex = player.getPlayerIndex();
		
		if((playerIndex  < 0 ) || (playerIndex  >= pitHolders.size())) 
			throw new InvalidSelectionException("Player is not valid");

		PitHolder activePlayersPitHolder = pitHolders.get(playerIndex);
		
		if((selectedPitIndex  < 0) || (selectedPitIndex >= activePlayersPitHolder.getPits().size()-1)) 
			throw new InvalidSelectionException("Selected Pit is not valid");
		
	}
}
