/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.List;

import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 * Game rule interface. abstraction of the rules for the game DefaultGameRule is default implementation
 */
public interface GameRule {

	/**
	 * After a player selects a pit to make a move, how the stones are distributed 
	 * across the pits should be implemented in this method.
	 * Returns the result of the Move in ExecutionResult which has the new active player
	 * @param pitHolders
	 * @param player
	 * @param selectedPitIndex
	 * @return
	 */
	ExecutionResult executeTheMove(List<PitHolder> pitHolders, GamePlayer player, int selectedPitIndex);
	
	/**
	 * Method to capture the opponents stones
	 * @param pitHolders
	 * @param player
	 * @param lastPitIndex
	 */
	void captureOpponentsStones(List<PitHolder> pitHolders, GamePlayer player, int lastPitIndex);
	

}
