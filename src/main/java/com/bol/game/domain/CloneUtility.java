/**
 * 
 */
package com.bol.game.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * @author Vishal
 *
 */
public class CloneUtility {

	public static Object clone(Object object){
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(),e.getCause());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e.getMessage(),e.getCause());
		}

	}
}
