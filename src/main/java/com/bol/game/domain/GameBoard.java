/**
 * 
 */
package com.bol.game.domain;

import com.bol.game.domain.player.GamePlayer;


/**
 * @author Vishal
 * Abstraction of the GameBoard, exposes various function for playing the game and quering the status of the game
 *
 */
public interface GameBoard {

	/**
	 * Executes the game rules after a player does a 'move' on the board
	 * @param player
	 * @param pitNum
	 */
	public void play(GamePlayer player, int selectedPit);
	
	/**
	 * Returns the current active player of the board
	 * @return
	 */
	public GamePlayer getActivePlayer();
	
	/**
	 * Returns the Board's current status
	 * @return
	 */
	public BoardStatus getBoardStatus();
	
	/**
	 * Returns true if the End of game has reached
	 * @return
	 */
	public boolean isEndOfGame();
	
	/**
	 * Returns the winner of the player after game ends
	 * @return
	 */
	public GamePlayer getWinner();

	/**
	 * Returns the winner of the player after game ends
	 * @return
	 */
	public GamePlayer[] getPlayers();
	
	/**
	 * Returns the identifier of the board
	 * @return
	 */
	public long getGameId();

}
