/**
 * 
 */
package com.bol.game.domain.pit;

import java.io.Serializable;

/**
 * Class that represents the Pit. Encapsulates all the pits properties & behavior.
 *
 */

public class Pit implements Serializable{

	private static final long serialVersionUID = -3225512276819232947L;

	int numOfStones;
	
	boolean bigPit;
	
	public Pit(int numOfStones, boolean isBigPit){
		this.numOfStones = numOfStones;
		this.bigPit = isBigPit;
	}
	
	public boolean isBigPit(){
		return this.bigPit;
	}
	
	public void setBigPit(boolean bigPit){
		this.bigPit = bigPit;
	}

	public void addStone(){
		this.numOfStones++;
	}

	public void addStone(int numOfStones){
		this.numOfStones =  this.numOfStones + numOfStones;
	}

	public int getNumOfStones(){
		return this.numOfStones;
	}
	/**
	 * Sets the numOfStones to 0 and returns 
	 * Return value is euqal to the original numOfStones 
	 * @return
	 */
	public int emptyThePit(){
		int numOfStones = this.numOfStones;
		this.numOfStones = 0;
		return numOfStones;
	}

	public boolean isEmpty(){
		return (numOfStones == 0);
	}
}
