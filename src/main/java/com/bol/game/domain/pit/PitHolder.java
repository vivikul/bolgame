/**
 * 
 */
package com.bol.game.domain.pit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bol.game.domain.player.GamePlayer;

/**
 * Container for holding the pits of a player. 
 * Always the last pit is a big pit
 */
public class PitHolder implements Serializable{

	private static final long serialVersionUID = 1948691018081809413L;

	private GamePlayer gamePlayer;
	
	private List<Pit> pits;

	/**
	 * Constructor with the number of pits to be created in the PitHolder for a Player
	 * Second parameter is integer denoting the number of stones placed in each small pit
	 * The last pit is marked as big pit, and number of stones is set to zero
	 * @param numOfPits
	 * @param numOfStonesInEachPit
	 */
	public PitHolder(int numOfPits, int numOfStonesInEachPit, GamePlayer gamePlayer) {
		
		pits = new ArrayList<Pit>(numOfPits);

		//initialize all the small pits
		for(int i =0 ; i < numOfPits - 1; i++){
			pits.add(new Pit(numOfStonesInEachPit,false));
		}

		//set last pit as big pit with 0 stones
		pits.add(new Pit(0,true));
		this.gamePlayer = gamePlayer;
	}

	/**
	 * Constructor with the number of pits to be created in the PitHolder for a Player
	 * Second parameter is the array of integer denoting the number of stones placed in each pit
	 * The last pit is marked as big pit, with stones equal to value in the last index of second parameter
	 * @param numOfPits
	 * @param numOfStonesInEachPit
	 */
	public PitHolder(int numOfPits, int[] numOfStonesInEachPit, GamePlayer gamePlayer) {
		pits = new ArrayList<Pit>(numOfPits);
		for(int i =0 ; i < numOfPits - 1; i++){
			pits.add(new Pit(numOfStonesInEachPit[i],false));
		}
		//set last pit as big pit
		pits.add(new Pit(numOfStonesInEachPit[numOfPits-1],true));
		this.gamePlayer = gamePlayer;
	}
	
	public Pit getPit(int index){
		return this.pits.get(index);
	}

	public List<Pit> getPits(){
		return this.pits;
	}

	/**
	 * returns true if all the small pits are empty
	 * @return
	 */
	public int getTotalNumOfStones(){
		int total=0;
		for(Pit pit : pits){
			total = total + pit.getNumOfStones();
		}
		return total;
	}


	/**
	 * @return the gamePlayer
	 */
	public GamePlayer getGamePlayer() {
		return gamePlayer;
	}


	/**
	 * @param gamePlayer the gamePlayer to set
	 */
	public void setGamePlayer(GamePlayer gamePlayer) {
		this.gamePlayer = gamePlayer;
	}
	
	/**
	 * returns true if all the small pits are empty
	 * @return
	 */
	public boolean areAllPitsEmpty(){
		for (Pit pit : pits){
			if( (!pit.isEmpty()) && (!pit.isBigPit())) return false;
		}
		return true;
	}

}
