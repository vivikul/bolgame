/**
 * 
 */
package com.bol.game.domain.exception;

/**
 * @author Vishal
 *
 */
public class InvalidSelectionException extends RuntimeException{

	private static final long serialVersionUID = -8341878496567496086L;

	public InvalidSelectionException(String message){
		super(message);
	}
}
