/**
 * 
 */
package com.bol.game.domain.exception;

/**
 * @author Vishal
 *
 */
public class BoardCorruptedException extends RuntimeException{

	private static final long serialVersionUID = -6784966068206128680L;

	public BoardCorruptedException(String message) {
		super(message);
	}
}
