/**
 * 
 */
package com.bol.game.domain.exception;

/**
 * @author Vishal
 *
 */
public class InvalidMoveException extends RuntimeException{

	private static final long serialVersionUID = -5696988271384201635L;

	public InvalidMoveException(String message) {
		super(message);
	}

	
	public InvalidMoveException(String message, Throwable cause) {
		super(message,cause);
	}

}
