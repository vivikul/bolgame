/**
 * 
 */
package com.bol.game.domain.exception;

/**
 * @author Vishal
 *
 */
public class BuilderException extends RuntimeException{

	private static final long serialVersionUID = -2523857462349557474L;

	public BuilderException(String message) {
		super(message);
	}
}
