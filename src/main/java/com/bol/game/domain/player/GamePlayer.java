/**
 * 
 */
package com.bol.game.domain.player;

import java.io.Serializable;

/**
 * @author Vishal
 *
 */
public class GamePlayer implements Serializable{

	private static final long serialVersionUID = 7063830117086025898L;

	/**
	 * Player number in the game
	 */
	private final int playerNumber;
	
	/**
	 * Player identity 
	 */
	private final Player player;
	
	public GamePlayer(int playerNum, String name) {
		this.playerNumber = playerNum;
		Player player = new Player();
		player.setName(name);
		this.player = player;
	}
	
	@Override
	public int hashCode() {
		return playerNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		return (this.playerNumber == ((GamePlayer)obj).playerNumber);
	}

	/**
	 * @return the playerNumber
	 */
	public int getPlayerNumber() {
		return playerNumber;
	}

	public Player getPlayer() {
		return player;
	}

	/**
	 * Returns the index of the player in the GameBoard
	 * Is always playerNumber - 1
	 * @return
	 */
	public int getPlayerIndex(){
		return playerNumber - 1;
	}
}
