/**
 * 
 */
package com.bol.game.domain.player;

import java.io.Serializable;

/**
 * @author Vishal
 *
 */
public class Player implements Serializable{

	private static final long serialVersionUID = -448152179105200789L;
	
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
