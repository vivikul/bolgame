/**
 * 
 */
package com.bol.game.model;

/**
 * @author Vishal
 *
 */
public class BoardSetup {

	private SetupEnum configuration;
	
	private int numOfPlayers;
	
	private int numOfPits;
	
	private int numOfStones;

	public int getNumOfPlayers() {
		return numOfPlayers;
	}

	public void setNumOfPlayers(int numOfPlayers) {
		this.numOfPlayers = numOfPlayers;
	}

	public int getNumOfPits() {
		return numOfPits;
	}

	public void setNumOfPits(int numOfPits) {
		this.numOfPits = numOfPits;
	}

	public int getNumOfStones() {
		return numOfStones;
	}

	public void setNumOfStones(int numOfStones) {
		this.numOfStones = numOfStones;
	}

	public SetupEnum getConfiguration() {
		return configuration;
	}

	public void setConfiguration(SetupEnum configuration) {
		this.configuration = configuration;
	}
	
}
