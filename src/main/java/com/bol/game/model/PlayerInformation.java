/**
 * 
 */
package com.bol.game.model;

/**
 * @author M06I546
 *
 */
public class PlayerInformation {
	
	private String name[];

	public String[] getName() {
		return name;
	}

	public void setName(String[] name) {
		this.name = name;
	}

	public String getName(int playernum) {
		return name[playernum - 1];
	}

	
}
