/**
 * 
 */
package com.bol.game.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bol.game.domain.BoardStatus;
import com.bol.game.domain.GameBoard;
import com.bol.game.domain.GameBoardBuilder;
import com.bol.game.model.BoardSetup;
import com.bol.game.model.PlayerInformation;

/**
 * @author Vishal
 * Web controller that maps the request URI's to the controller methods
 * 
 */

@Controller
public class GameWebController {
	
	/**
	 * displays the welcome page where the user can select the board configuration
	 * @param boardSetup
	 * @return
	 */
	@RequestMapping("/welcome")
	public String setup(final BoardSetup boardSetup){
        return "welcome";
	}

	/**
	 * Displays the second page, the user has to fill in the details of the players on this screen
	 * @param boardSetup
	 * @param bindingResult
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("/initialize")
	public String initialize(final BoardSetup boardSetup, final BindingResult bindingResult, 
			final ModelMap model, HttpSession session){
        
		GameBoardBuilder gameBoardBuilder = new GameBoardBuilder();
		session.setAttribute("boardBuilder", gameBoardBuilder);
		PlayerInformation pInformation = new PlayerInformation();
		model.addAttribute(pInformation);
		model.addAttribute("tempGameId",System.currentTimeMillis());
		return "players";
	}

	/**
	 * This call adds the players information to the Game board
	 * @param gameId
	 * @param playerInformation
	 * @param bindingResult
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("/addPlayers/{tempGameId}")
	public String addPlayers(@PathVariable(value="tempGameId") String gameId, final PlayerInformation playerInformation, final BindingResult bindingResult, 
			final ModelMap model, HttpSession session){
        
		//TODO validate & authenticate the {tempGameId}
		
		GameBoardBuilder gameBoardBuilder = (GameBoardBuilder)session.getAttribute("boardBuilder");
		
		gameBoardBuilder.setGamePlayer(playerInformation.getName(1), 1);
		gameBoardBuilder.setGamePlayer(playerInformation.getName(2), 2);
		
		GameBoard gameBoard = gameBoardBuilder.build();
		session.setAttribute("gameBoard", gameBoard);
		model.addAttribute(gameBoard.getBoardStatus());
		return "gameBoard";
	}

	/**
	 * executes the move by the player
	 * @param gameId
	 * @param player
	 * @param selectedPit
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("/play/{gameId}/{player}/{selectedPit}")
	public String play(@PathVariable(value="gameId") String gameId,
					@PathVariable(value="player") int player,@PathVariable(value="selectedPit") int selectedPit,
					final ModelMap model, HttpSession session){
        
		GameBoard board = (GameBoard)session.getAttribute("gameBoard");
		board.play(board.getPlayers()[player - 1], selectedPit);
		return "redirect:/play/showBoard/" + gameId;
	}

	/**
	 * Displays the current statue of the game
	 * @param gameId
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("/play/showBoard/{gameId}")
	public String showBoard(@PathVariable(value="gameId") String gameId,final ModelMap model, 
			HttpSession session){
		
		GameBoard board = (GameBoard)session.getAttribute("gameBoard");
		BoardStatus boardStatus = board.getBoardStatus();
		model.addAttribute(boardStatus);
		return "gameBoard";
	}

	
}
