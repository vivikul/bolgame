/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author Vishal
 *
 */
public class DefaultGameRuleTest {

	GameRule gameRule = new DefaultGameRule();
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Positive test
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * last pit is 0
	 * Expected output is [{6,7,7,7,7,7,1},{6,6,6,6,6,0,0}] 
	 */
	@Test
	public void testCaptureOpponentsStones() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		pitHolders.add(new PitHolder(7, new int[]{0,7,7,7,7,7,1},getPlayer(1)));
		pitHolders.add(new PitHolder(7, new int[]{6,6,6,6,6,6,0},getPlayer(2)));
		gameRule.captureOpponentsStones(pitHolders, new GamePlayer(1,"Vishal"), 0);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);

		Assert.assertTrue(pitHolders.get(1).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(1).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(2).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(3).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(4).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(5).getNumOfStones() == 0);
		Assert.assertTrue(pitHolders.get(1).getPit(6).getNumOfStones() == 0);

	}

	/**
	 * Positive test
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 2th
	 * last pit is 5
	 * Expected output is [{0,7,7,7,7,7,1},{6,6,6,6,6,8,0}] 
	 */
	@Test
	public void testCaptureOpponentsStones1() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		pitHolders.add(new PitHolder(7, new int[]{2,7,7,7,7,7,1},getPlayer(1)));
		pitHolders.add(new PitHolder(7, new int[]{6,6,6,6,6,6,0},getPlayer(2)));
		gameRule.captureOpponentsStones(pitHolders, new GamePlayer(2,"Vishal"), 5);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 0);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);

		Assert.assertTrue(pitHolders.get(1).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(1).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(2).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(3).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(4).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(5).getNumOfStones() == 8);
		Assert.assertTrue(pitHolders.get(1).getPit(6).getNumOfStones() == 0);

	}

	/**
	 * Positive test
	 * if the last stone lands in the big pit the active player retains the move
	 * LastPitBig = true, lastPitempty = false
	 * player active  = player 1
	 * New player active = player 1
	 */
	@Test
	public void testExecuteTheMove() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(true,false,0));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		GamePlayer player1 = getPlayer(1);
		GamePlayer player2 = getPlayer(2);
		pitHolders.add(new PitHolder(7, config,player1));
		pitHolders.add(new PitHolder(7,6,player2));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, player1, 0);
		Assert.assertTrue(result.getNewActivePlayer().equals(player1));
	}

	/**
	 * Positive test
	 * if the last stone lands in the small pit the next player becomes active
	 * LastPitBig = false, lastPitempty = false
	 * player active  = player 1
	 * New player active = player 2
	 */
	@Test
	public void testExecuteTheMove1() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(false,false,0));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		GamePlayer player1 = getPlayer(1);
		GamePlayer player2 = getPlayer(2);
		pitHolders.add(new PitHolder(7, config,player1));
		pitHolders.add(new PitHolder(7,6,player2));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, player1, 0);
		Assert.assertTrue(result.getNewActivePlayer().equals(player2));
	}

	/**
	 * Positive test
	 * if the last stone lands in the small pit the next player becomes active
	 * LastPitBig = false, lastPitempty = false
	 * player active  = player 2
	 * New player active = player 1
	 */
	@Test
	public void testExecuteTheMove3() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(false,false,0));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		GamePlayer player1 = getPlayer(1);
		GamePlayer player2 = getPlayer(2);
		pitHolders.add(new PitHolder(7, config,player1));
		pitHolders.add(new PitHolder(7,6,player2));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, player2, 0);
		Assert.assertTrue(result.getNewActivePlayer().equals(player1));
	}

	/**
	 * Positive test
	 * if the last stone lands in an empty small pit stones from opponent are captured
	 * LastPitBig = false, lastPitempty = true
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 2th
	 * last pit is 5
	 * Expected output is [{0,7,7,7,7,7,1},{6,6,6,6,6,8,0}] 
	 */
	@Test
	public void testExecuteTheMove4() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(false,true,5));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		pitHolders.add(new PitHolder(7, new int[]{2,7,7,7,7,7,1},getPlayer(1)));
		pitHolders.add(new PitHolder(7, new int[]{6,6,6,6,6,6,0},getPlayer(2)));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, getPlayer(2), 0);

		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 0);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);

		Assert.assertTrue(pitHolders.get(1).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(1).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(2).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(3).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(4).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(5).getNumOfStones() == 8);
		Assert.assertTrue(pitHolders.get(1).getPit(6).getNumOfStones() == 0);

		
	}


	/**
	 * Positive test
	 * if the last stone lands in an empty big pit stones from opponent are not captured
	 * LastPitBig = true, lastPitempty = true
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 2th
	 * last pit is 5
	 * Expected output is [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}] 
	 */
	@Test
	public void testExecuteTheMove5() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(true,true,5));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		pitHolders.add(new PitHolder(7, new int[]{2,7,7,7,7,7,1},getPlayer(1)));
		pitHolders.add(new PitHolder(7, new int[]{6,6,6,6,6,6,0},getPlayer(2)));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, getPlayer(2), 0);

		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 2);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);

		Assert.assertTrue(pitHolders.get(1).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(1).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(2).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(3).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(4).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(5).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(6).getNumOfStones() == 0);

		
	}

	/**
	 * Positive test
	 * if the last stone lands in an non empty pit, stones from opponent are not captured
	 * LastPitBig = false, lastPitempty = false
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 2th
	 * last pit is 5
	 * Expected output is [{2,7,7,7,7,7,1},{6,6,6,6,6,6,0}] 
	 */
	@Test
	public void testExecuteTheMove6() {
		StoneDistributionStrategy strategy = mock(StoneDistributionStrategy.class);
		when(strategy.shuffleStones(any(List.class), any(GamePlayer.class), any(Integer.class)))
										.thenReturn(getStoneDistributionResult(false,false,5));
		((DefaultGameRule)gameRule).setStoneDistributionStrategy(strategy);

		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		pitHolders.add(new PitHolder(7, new int[]{2,7,7,7,7,7,1},getPlayer(1)));
		pitHolders.add(new PitHolder(7, new int[]{6,6,6,6,6,6,0},getPlayer(2)));

		ExecutionResult result = gameRule.executeTheMove(pitHolders, getPlayer(2), 0);

		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 2);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);

		Assert.assertTrue(pitHolders.get(1).getPit(0).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(1).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(2).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(3).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(4).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(5).getNumOfStones() == 6);
		Assert.assertTrue(pitHolders.get(1).getPit(6).getNumOfStones() == 0);
	}

	
	private GamePlayer getPlayer(int playernumber){
		GamePlayer gamePlayer = new GamePlayer(playernumber, "vishal");
		return gamePlayer;
	}

	private StoneDistributionResult getStoneDistributionResult(boolean bigPit, boolean empty, int lastPitIndex){
		StoneDistributionResult result = new StoneDistributionResult();
		result.setLastPitBig(bigPit);
		result.setLastPitempty(empty);
		result.setLastPitIndex(lastPitIndex);
		return result;
	}
}
