/**
 * 
 */
package com.bol.game.domain.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.bol.game.domain.exception.InvalidSelectionException;
import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

/**
 * @author m06i546
 *
 */
public class StoneDistributionStrategy1Test {

	private StoneDistributionStrategy1 stoneDistributionStrategy1 = new StoneDistributionStrategy1();
	
	@Test
	/**
	 * Normal test
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{6,6,6,6,6,6,0},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * Oth pit selected
	 * Number of stones in the selected pit 6
	 * Expected output is  [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * last pit is big
	 * last pit is empty
	 */
	public void testShuffleStones1() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		pitHolders.add(new PitHolder(7,6,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		StoneDistributionResult result = stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 0);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 0);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(result.isLastPitBig());
		Assert.assertTrue(result.isLastPitempty());
	}

	@Test
	/**
	 * Normal test
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * 1th pit selected
	 * Expected output is [{1,1,8,8,8,8,2},{6,6,6,6,6,6,0}] 
	 * last pit is not big
	 * last pit is empty
	 */
	public void testShuffleStones2() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {0,7,7,7,7,7,1};
		pitHolders.add( new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		StoneDistributionResult result = stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 1);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 1);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 2);
		
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 1);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 8);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 8);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 8);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 8);
		Assert.assertFalse(result.isLastPitBig());
		Assert.assertTrue(result.isLastPitempty());
	}

	@Test
	/**
	 * Normal test
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [[{1,1,8,8,8,8,2},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * 6th pit selected
	 * Expected output is [{2,2,9,9,9,1,4},{6,6,6,6,6,6,0}] 
	 * last pit is not big
	 * last pit is empty
	 */
	public void testShuffleStones3() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {1,1,8,8,8,8,2};
		pitHolders.add(new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		StoneDistributionResult result = stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 5);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 2);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 2);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 9);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 9);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 9);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 1);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 4);
		
		Assert.assertTrue(result.isLastPitBig());
		Assert.assertFalse(result.isLastPitempty());
	}

	@Test
	/**
	 * Pit with zero stones selected
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * 0th pit selected
	 * Expected output is [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}] 
	 * last pit is not big
	 * last pit is not empty
	 * effectively no change
	 */
	public void testShuffleStones4() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {0,7,7,7,7,7,1};
		pitHolders.add(new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		StoneDistributionResult result = stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 0);
		Assert.assertTrue(pitHolders.get(0).getPit(0).getNumOfStones() == 0);
		Assert.assertTrue(pitHolders.get(0).getPit(1).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(2).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(3).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(4).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(5).getNumOfStones() == 7);
		Assert.assertTrue(pitHolders.get(0).getPit(6).getNumOfStones() == 1);
		
		Assert.assertFalse(result.isLastPitBig());
		Assert.assertFalse(result.isLastPitempty());
	}

	@Test(expected = InvalidSelectionException.class) 
	/**
	 * Exception condition
	 * index of pit selected is out of array range
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * 7th pit selected
	 * Expected output is InvalidInputException 
	 */
	public void testShuffleStonesException1() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {0,7,7,7,7,7,1};
		pitHolders.add(new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 7);
	}

	@Test(expected = InvalidSelectionException.class) 
	/**
	 * Exception condition
	 * player has not pitHolder
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 0th
	 * 7th pit selected
	 * Expected output is InvalidInputException 
	 */
	public void testShuffleStonesException2() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {0,7,7,7,7,7,1};
		pitHolders.add( new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), 7);
	}

	@Test(expected = InvalidSelectionException.class) 
	/**
	 * Exception condition
	 * index of pit selected is -ve
	 * number of PitHolers in array is 2
	 * number of pits in the PitHolders is 6
	 * [{0,7,7,7,7,7,1},{6,6,6,6,6,6,0}]
	 * player selected is 1th
	 * 6th pit selected
	 * Expected output is InvalidInputException 
	 */
	public void testShuffleStonesException3() {
		List<PitHolder> pitHolders = new ArrayList<PitHolder>(2);
		int[] config = {0,7,7,7,7,7,1};
		pitHolders.add(new PitHolder(7, config,getPlayer(1)));
		pitHolders.add(new PitHolder(7,6,getPlayer(2)));
		
		stoneDistributionStrategy1.shuffleStones(pitHolders, new GamePlayer(1,"Vishal"), -1);
	}

	
	private GamePlayer getPlayer(int playernumber){
		GamePlayer gamePlayer = new GamePlayer(playernumber, "vishal");
		return gamePlayer;
	}
}
