package com.bol.game.domain;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;

import com.bol.game.domain.exception.BoardCorruptedException;
import com.bol.game.domain.exception.InvalidMoveException;
import com.bol.game.domain.exception.InvalidSelectionException;
import com.bol.game.domain.pit.PitHolder;
import com.bol.game.domain.player.GamePlayer;

public class DefaultGameBoardTest {

	DefaultGameBoard board;
	
	GameBoardBuilder builder;
	
	Field pitHoldersField;
	
	List<PitHolder> pitHolders;
	
	@Before
	public void initTest() throws NoSuchFieldException, IllegalAccessException{
		GameBoardBuilder builder = new GameBoardBuilder();
		builder.setGamePlayer("vishal", 1);
		builder.setGamePlayer("vishal1", 2);
		board = (DefaultGameBoard)builder.build();
		pitHoldersField = DefaultGameBoard.class.getDeclaredField("pitHolders");
		pitHoldersField.setAccessible(true);
		
		pitHolders = (List<PitHolder>)pitHoldersField.get(board);
	}
	@Test
	/**
	 * Returns the winner of the game
	 * if player 1 has {6,6,6,6,6,6,0} and 
	 * player 2 has {0,0,0,0,0,0,20}
	 * Player1 is winner
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testGetWinner() {

		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		pitHolder2.getPit(6).addStone(20);
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		Assert.isTrue(board.getWinner().equals(new GamePlayer(1, "Vishal")));
	}

	@Test
	/**
	 * Returns the winner of the game
	 * if player 2 has {6,6,6,6,6,6,0} and 
	 * player 1 has {0,0,0,0,0,0,37}
	 * Player1 is winner
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testGetWinner1() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(2, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(1, "Vishal1"));
		pitHolder2.getPit(6).addStone(37);
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		Assert.isTrue(board.getWinner().equals(new GamePlayer(1, "Vishal")));
	}

	@Test
	/**
	 * Returns null for winner of the game has not reached the end
	 * if player 1 has {0,0,1,0,0,0,54} and 
	 * player 2 has {6,0,0,0,0,0,50}
	 * result is null
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testGetWinner3() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 2, new GamePlayer(2, "Vishal1"));
		
		pitHolder1.getPit(0).addStone(0);
		pitHolder1.getPit(1).addStone(0);
		pitHolder1.getPit(2).addStone(1);
		pitHolder1.getPit(3).addStone(0);
		pitHolder1.getPit(4).addStone(0);
		pitHolder1.getPit(5).addStone(0);
		pitHolder1.getPit(5).addStone(54);

		pitHolder2.getPit(0).addStone(6);
		pitHolder2.getPit(1).addStone(0);
		pitHolder2.getPit(2).addStone(0);
		pitHolder2.getPit(3).addStone(0);
		pitHolder2.getPit(4).addStone(0);
		pitHolder2.getPit(5).addStone(0);
		pitHolder2.getPit(5).addStone(50);
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		Assert.isTrue(board.getWinner() == null);
	}

	@Test
	/**
	 * Returns true is one player has all the small pits empty 
	 * if player 1 has {6,6,6,6,6,6,0} and 
	 * player 2 has {0,0,0,0,0,0,0}
	 * Expected result true
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testIsEndOfGame() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		Assert.isTrue(board.isEndOfGame());
	}

	@Test
	/**
	 * Returns true is one player has all the small pits empty 
	 * if player 1 has {6,6,6,6,6,6,0} and 
	 * player 2 has {2,2,2,2,2,2,0}
	 * Expected result false
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testIsEndOfGame1() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		Assert.isTrue(board.isEndOfGame());
	}

	@Test(expected = InvalidMoveException.class)
	/**
	 * Validate input for move
	 * Pass player 2 when Player 1 is active
	 * Expected result InvaildMoveException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testPlayExcpetion() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		board.play(new GamePlayer(2, "Vishal1"), 4);
	}

	@Test(expected = BoardCorruptedException.class)
	/**
	 * Validate input for move
	 * The board is corrupted
	 * PitHolder in index 0 of pitHolders has a player 2 instead of player 1
	 * Expected result BoardCorruptedException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testPlayExcpetion1() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder2);
		pitHolders.add(1,pitHolder1);
		board.play(new GamePlayer(1, "Vishal1"), 4);
	}

	@Test(expected = InvalidSelectionException.class)
	/**
	 * Validate input for move
	 * If selected pit index is > than the number of small pits in a players pitholder
	 * PitHolder in index 0 of pitHolders has a player 2 instead of player 1
	 * Expected result InvalidSelectionException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testPlayExcpetion2() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		board.play(new GamePlayer(1, "Vishal1"), 6);
	}

	@Test(expected = InvalidSelectionException.class)
	/**
	 * Validate input for move
	 * If selected pit index is > than the number of small pits in a players pitholder
	 * PitHolder in index 0 of pitHolders has a player 2 instead of player 1
	 * Expected result InvalidSelectionException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testPlayExcpetion3() {
		
		PitHolder pitHolder1 = new PitHolder(7, 6, new GamePlayer(1, "Vishal"));
		PitHolder pitHolder2 = new PitHolder(7, 0, new GamePlayer(2, "Vishal1"));
		
		pitHolders.clear();
		pitHolders.add(0,pitHolder1);
		pitHolders.add(1,pitHolder2);
		board.play(new GamePlayer(1, "Vishal1"), -1);
	}

}
