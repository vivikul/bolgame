package com.bol.game.domain;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Test;
import org.springframework.util.Assert;

import com.bol.game.domain.exception.BuilderException;
import com.bol.game.domain.pit.PitHolder;

public class GameBoardBuilderTest {

	GameBoardBuilder builder;
	
	@Test
	/**
	 * Positive test for testing the build method
	 * PitHolders must be initialized
	 * The 1st player should be set to active
	 * GameId is set to !- 0
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public void testSetGamePlayer() throws NoSuchFieldException, IllegalAccessException{
		builder = new GameBoardBuilder();
		builder.setGamePlayer("Vishal", 1);
		builder.setGamePlayer("Alex", 2);
		DefaultGameBoard board = (DefaultGameBoard)builder.build();
		
		Field pitHoldersField = DefaultGameBoard.class.getDeclaredField("pitHolders");
		pitHoldersField.setAccessible(true);
		
		List<PitHolder> pitHolders = (List<PitHolder>)pitHoldersField.get(board);
		
		Assert.isTrue(pitHolders.size() == 2);
		Assert.isTrue(pitHolders.get(0) != null);
		Assert.isTrue(pitHolders.get(1) != null);
		Assert.isTrue(board.getActivePlayer().getPlayerNumber() == 1);
		Assert.isTrue(board.getGameId() != 0);
	}

	/**
	 * Exception
	 * Number of players set < 2
	 * The 1st player should be set to active
	 * GameId is set to !- 0
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test(expected = BuilderException.class)
	public void testSetGamePlayerException1() throws NoSuchFieldException, IllegalAccessException{
		builder = new GameBoardBuilder();
		builder.setGamePlayer("Vishal", 1);
		DefaultGameBoard board = (DefaultGameBoard)builder.build();
		
	}

	/**
	 * Exception
	 * Number of players set > 2
	 * The 1st player should be set to active
	 * GameId is set to !- 0
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test(expected = BuilderException.class)
	public void testSetGamePlayerException2() throws NoSuchFieldException, IllegalAccessException{
		builder = new GameBoardBuilder();
		builder.setGamePlayer("Vishal", 1);
		builder.setGamePlayer("Vishal1", 2);
		builder.setGamePlayer("Vishal2", 3);
		DefaultGameBoard board = (DefaultGameBoard)builder.build();
		
	}

}
